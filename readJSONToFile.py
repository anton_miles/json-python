#!/bin/env python3

#------------------------------------------------------------------------------
# File: readJSONToFile.py
#
# Author: Anton Miles	Date Last Modified: 4/26/2014
#
# Python script to build a text file called "states.dat" populated by data 
# fetched from "http://api.sba.gov/geodata/city_county_links_for_state_of/"
# for each state in states[].
#------------------------------------------------------------------------------
import urllib.request
from urllib.parse import urljoin
import json

states = [#list of each states abbreviation
"AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
"HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
"MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
"NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
"SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"
]
output = open("states.dat", "w")#open file to be written
#the path to the information except for the state shorthand and extension
basePath = "http://api.sba.gov/geodata/city_county_links_for_state_of/"
jsonExt = ".json"#extension to be added at the end of the url
printText = ''#empty string to use as print medium

try:
	for state in states:#iterate through the states list
		output.write(state + "\n")#header for searching
		localPath = state.lower() + jsonExt#build relative location 
		wholePath = urljoin(basePath, localPath)#create absolute path
		response = urllib.request.urlopen(wholePath)#open url at wholePath
		#convert data to python dictionary type
		keyPairs = json.loads(response.read().decode())
		#build string of dictionary key/values
		text = '\n'.join(map(str, keyPairs))
		lines = text.strip().splitlines()#strip whitespace, split on lines
		for piece in lines:# for each split piece...
			#...strip characters and split on comma and pass pieces to next for
			subPieces = piece.strip("[{}]\t\n").split(",")
			for chunk in subPieces:
				# build base print file. Tab for readability, data, 
				# then semicolon
				finalPieces = chunk.strip().split(": ", 1)
				if len(finalPieces) >= 2:
					#add to print string, the 2 pieces seperated by '=' and 
					#ending with a semicolon and tab
					printText += finalPieces[0] + " = " + finalPieces[1] + ' ;\t'
				else:#to avoid argument out of range problems
					printText += finalPieces[0]
			printText = printText + "\n"
		printText = printText.replace("'", "")
		output.write(printText + "\n")#write string to file and new line feed
		printText = ''
except Exception as e:#Catch obscure unexpected errors
	raise # raise to help find error origin
finally:
   output.close()#even if it errors above, close the file
